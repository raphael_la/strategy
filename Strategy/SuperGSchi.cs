﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    class SuperGSchi : ISchi
    {
        public Image ShowSchi()
        {
            return WifiSchi.SchiVerleih.GetSchi(WifiSchi.SchiTyp.SuperG);
        }
    }
}
