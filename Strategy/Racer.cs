﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    class Racer
    {
        private ISchi schi;
        public void SetSchi(ISchi schi)
        {
            this.schi = schi;
        }

        public Image Start()
        {
            return schi.ShowSchi();
        }
    }
}
