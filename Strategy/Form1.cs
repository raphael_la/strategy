﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strategy
{
    public partial class Form1 : Form
    {

        Racer r = new Racer();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {           
            if (optRTL.Checked)
                r.SetSchi(new RTLSchi());
            else if (optSG.Checked)
                r.SetSchi(new SuperGSchi());
            else
                r.SetSchi(new SlalomSchi());
            
            this.pictureBox1.Image = r.Start();  
        }
    }
}
