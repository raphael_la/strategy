﻿namespace Strategy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.optSlalom = new System.Windows.Forms.RadioButton();
            this.optRTL = new System.Windows.Forms.RadioButton();
            this.optSG = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 570);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // optSlalom
            // 
            this.optSlalom.AutoSize = true;
            this.optSlalom.Checked = true;
            this.optSlalom.Location = new System.Drawing.Point(329, 16);
            this.optSlalom.Name = "optSlalom";
            this.optSlalom.Size = new System.Drawing.Size(56, 17);
            this.optSlalom.TabIndex = 1;
            this.optSlalom.TabStop = true;
            this.optSlalom.Text = "Slalom";
            this.optSlalom.UseVisualStyleBackColor = true;
            // 
            // optRTL
            // 
            this.optRTL.AutoSize = true;
            this.optRTL.Location = new System.Drawing.Point(329, 39);
            this.optRTL.Name = "optRTL";
            this.optRTL.Size = new System.Drawing.Size(46, 17);
            this.optRTL.TabIndex = 2;
            this.optRTL.TabStop = true;
            this.optRTL.Text = "RTL";
            this.optRTL.UseVisualStyleBackColor = true;
            // 
            // optSG
            // 
            this.optSG.AutoSize = true;
            this.optSG.Location = new System.Drawing.Point(329, 62);
            this.optSG.Name = "optSG";
            this.optSG.Size = new System.Drawing.Size(40, 17);
            this.optSG.TabIndex = 3;
            this.optSG.TabStop = true;
            this.optSG.Text = "SG";
            this.optSG.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(329, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 582);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.optSG);
            this.Controls.Add(this.optRTL);
            this.Controls.Add(this.optSlalom);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton optSlalom;
        private System.Windows.Forms.RadioButton optRTL;
        private System.Windows.Forms.RadioButton optSG;
        private System.Windows.Forms.Button button1;
    }
}

